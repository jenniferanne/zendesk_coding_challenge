import requests
import getpass
import base64
import math
import sys
from cmd import Cmd

def get_input(text, sensitive=False):
  if sensitive == False:
    return input(text)
  else:
    return getpass.getpass(text)

 
class TicketViewer(Cmd):

  prompt = '\n >> '
  intro = 'Welcome to Ticket Viewer! Type "login" to log in'
  outro = 'Thanks for using Ticket Viewer!'
  no_data_error = 'Cannot use this command yet, as no user is logged in.'
  subdomain = None
  headers = None
  previous_page = None
  current_page_no = None
  next_page = None
  data = None
  page_count = None

  def do_exit(self, inp):
    print(self.outro)
    return 'Exited successfully'
  
  def help_exit(self):
    print('Exits the application.')

  def do_login(self, inp):
    self.subdomain = get_input("Please enter the subdomain for your account: ").strip()
    while self.subdomain == '':
      print('It looks like the subdomain you entered was blank. Please try again.')
      self.subdomain = get_input("Please enter the subdomain for your account: ").strip()
    url = getInitialURL(self.subdomain)
    user = get_input("Please enter the email address for your account: ").strip()
    while user == '':
      print('It looks like the email address you entered was blank. Please try again.')
      user = get_input("Please enter the email address for your account: ").strip()
    pwd = get_input("Please enter the password for your account: ", True)
    while pwd == '':
      print('It looks like the password you entered was blank. Please try again.')
      pwd = get_input("Please enter the password for your account: ", True)
    self.headers = createHeaders(user, pwd)
    pwd = "default"
    self.data = getData(url, self.headers)
    if self.data != None:
      self.current_page_no = 1
      self.next_page = self.data['next_page']
      self.page_count = math.ceil(self.data['count']/25.0)
      print('\nSuccessfully logged in to %s\'s account.' % user)
      print('Please choose one of the following options:')
      print('    "all" to list all tickets')
      print('    "view" to view a specific ticket')
      print('    "exit" to terminate the program')

  def help_login(self):
    print("Logs in to a zendesk account.")

  def do_all(self, inp):
    if self.data == None:
      print(self.no_data_error)
    else:
      displayPage(self.current_page_no, self.page_count, self.data)

  def help_all(self):
    print('Displays all tickets on the current page.')

  def do_next(self, inp):    
    if self.data == None:
      print(self.no_data_error)
    else: 
      if self.current_page_no == self.page_count:
        print('Cannot load the next page, as you are already on the last page.')
      else:
        self.current_page_no += 1
        url = self.next_page
        self.data = getData(url, self.headers)
        if self.data != None:
          self.next_page = self.data['next_page']
          self.previous_page = self.data['previous_page']
          displayPage(self.current_page_no, self.page_count, self.data)

  def help_next(self):
    print('Displays the next page of tickets.')

  def do_prev(self, inp):
    if self.data == None:
      print(self.no_data_error)
    else: 
      if self.current_page_no == 1:
        print('Cannot load the previous page, as you are already on the first page.')
      else:
        self.current_page_no -= 1
        url = self.previous_page
        self.data = getData(url, self.headers)
        if self.data != None:
          self.next_page = self.data['next_page']
          self.previous_page = self.data['previous_page']
          displayPage(self.current_page_no, self.page_count, self.data)

  def help_prev(self):
    print('Displays the previous page of tickets.')

  def do_view(self, inp):
    if self.subdomain == None:
      print(self.no_data_error)
    else: 
      id_no = inp
      if id_no == '':
        id_no = get_input('Please type the id number of the ticket you wish to view: ')
      viewTicket(id_no, self.subdomain, self.headers)
      print('\nPlease choose one of the following options:')
      print('    "all" to list all tickets on current page')
      printNextPrev(self.current_page_no, self.page_count)

  def help_view(self):
    print('Views a ticket on the current page.')

  def default(self, inp):
    print("Sorry, I don't know that command.")
    print("Please try again or type ? to list commands.")
   
  def do_EOF(self, inp):
    return True
  help_EOF = help_exit

# Given either an base64 encoded user:pass string or a user and pass,
# creates a Basic Authorization header
def createHeaders(user_or_auth, pwd=None):
  headers = None
  if pwd == None:
    headers = {'Authorization': 'Basic %s' % user_or_auth}
  else:
    auth_string = base64.b64encode(('%s:%s' % (user_or_auth, pwd)).encode('ascii')).decode('ascii')
    headers = {'Authorization': 'Basic %s' % auth_string}
  return headers

###########################################################################


# Creates the url for the first page of tickets
def getInitialURL(subdomain):
  url = 'https://%s.zendesk.com/api/v2/tickets.json?per_page=25' % subdomain
  return url

# Uses the given url and headers to retrieve data or return an error message
def getData(url, headers):
  response = requests.get(url, headers=headers)
  data = None
  if response.status_code != 200:
    print(errorMessage(response.status_code))
    print('Type "login" to try logging in again.')
  else:
    data = response.json()
  return data

# Return custom error messages for HTTP codes
def errorMessage(code):
  switch = {
    401: "Sign-in failed. Please check your username and password are correct.",
    403: "Sorry, this user doesn't have the required permissions to use the API.",
    404: "Sign-in failed. Please check you entered the correct subdomain.",
    429: "Rate limit has been exceeded."
  }
  return switch.get(code, "Sorry, an unknown error occurred.")

def displayPage(current_page_no, page_count, data):
  print('\nNow viewing page %i of %i:\n' % (current_page_no, page_count))
  print('%-6s%-50s%-25s' % ('ID#', 'Subject', 'Creation date'))
  print('-'*80)
  requests_list = data['tickets']
  for request in requests_list:
    date_string = request['created_at'].replace('T', ' ').replace('Z', ' UTC')
    print('%-6i%-50s%-25s' % (request['id'], request['subject'], date_string))
  print('\nPlease choose one of the following options:')
  printNextPrev(current_page_no, page_count)

def printNextPrev(current_page_no, page_count):
  if page_count > current_page_no:
    print('    "next" to list the next page of tickets')
  if current_page_no is not 1:
    print('    "prev" to list the previous page of tickets')
  print('    "view" to view details for a specific ticket')  
  print('    "exit" to terminate the program')

def viewTicket(id_no, subdomain, headers):
  url = 'https://' + subdomain + '.zendesk.com/api/v2/search.json?query=type%3Aticket%20' + id_no
  data = getData(url, headers)
  if data != None:
    if data['count'] is 0:
      print('No ticket with that id was found.')
    else:
      ticket = data['results'][0]
      date_string = ticket['created_at'].replace('T', ' ').replace('Z', ' UTC')
      print('Ticket created at %s by %i: \n' % (date_string, ticket['submitter_id']))
      print('Subject: %s \n' % ticket['subject'])
      print(ticket['description'])


if __name__ == '__main__':
  TicketViewer().cmdloop()


