import sys
import ticket_viewer
from mock import patch
from unittest import TestCase
from contextlib import contextmanager
from io import StringIO

intro = ticket_viewer.TicketViewer().intro
outro = ticket_viewer.TicketViewer().outro

@contextmanager
def captured_output():
  new_out, new_err = StringIO(), StringIO()
  old_out, old_err = sys.stdout, sys.stderr
  try:
    sys.stdout, sys.stderr = new_out, new_err
    yield sys.stdout, sys.stderr
  finally:
    sys.stdout, sys.stderr = old_out, old_err

def test_errorMessage_401():
  assert ticket_viewer.errorMessage(401) == "Sign-in failed. Please check your username and password are correct."

def test_errorMessage_default():
  assert ticket_viewer.errorMessage(500) == "Sorry, an unknown error occurred."

def test_createHeaders():
  actual = ticket_viewer.createHeaders("sample@emailaddress.com", "mypassword")
  expected = "{'Authorization': 'Basic c2FtcGxlQGVtYWlsYWRkcmVzcy5jb206bXlwYXNzd29yZA=='}"
  assert str(actual) == expected

def test_createHeaders_from_auth():
  actual = ticket_viewer.createHeaders("c2FtcGxlQGVtYWlsYWRkcmVzcy5jb206bXlwYXNzd29yZA==")
  expected = "{'Authorization': 'Basic c2FtcGxlQGVtYWlsYWRkcmVzcy5jb206bXlwYXNzd29yZA=='}"
  assert str(actual) == expected

def test_initial_url():
  actual = ticket_viewer.getInitialURL('subdomain')
  expected = 'https://subdomain.zendesk.com/api/v2/tickets.json?per_page=25'
  assert actual == expected


class SimulatedInput:

    def __init__(self,*args):
        self.args = iter(args) 

    def __call__(self,x):
        try:
            return next(self.args)
        except StopIteration:
            raise Exception("No more input")


class Test(TestCase):
  @patch('builtins.input', return_value = 'exit')
  def test_exit(self, mock_raw_input):
#    mock_raw_input.return_value = 'exit'
    with captured_output() as (out, err):
      ticket_viewer.TicketViewer().cmdloop()
    output = out.getvalue().strip().split('\n')
    assert output[0] == intro
    assert output[-1] == outro


  @patch('builtins.input', side_effect=['all','next', 'prev','view','view 35','exit'])
  def test_sequence_no_login(self, mock_raw_input):
    with captured_output() as (out, err):
      ticket_viewer.TicketViewer().cmdloop()
    output = out.getvalue().strip().split('\n')
    assert output[0] == intro
    assert output[-1] == outro
