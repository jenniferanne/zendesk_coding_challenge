import sys
import ticket_viewer
from mock import patch
from contextlib import contextmanager
from io import StringIO

intro = ticket_viewer.TicketViewer().intro
outro = ticket_viewer.TicketViewer().outro
# Login details used for testing. Currently set to placeholders so as not to store my password on git
# Before testing, please replace these placeholders with valid login details.
login_details = ['{subdomain}','{email_address}', '{password}']

@contextmanager
def captured_output():
  new_out, new_err = StringIO(), StringIO()
  old_out, old_err = sys.stdout, sys.stderr
  try:
    sys.stdout, sys.stderr = new_out, new_err
    yield sys.stdout, sys.stderr
  finally:
    sys.stdout, sys.stderr = old_out, old_err

def pytest_generate_tests(metafunc):
    idlist = []
    argvalues = []
    argnames = []
    for path in metafunc.cls.paths:
      idlist.append(path[0])
      items = path[1].items()
      argnames = [x[0] for x in items]
      argvalues.append(([x[1] for x in items]))
    metafunc.parametrize(argnames, argvalues, ids=idlist, scope="class")

path1 = ("login_all_exit", {'raw_input_list':['login','all','exit'],
  'input_list':login_details})
path2 = ("too_many_nexts_and_prevs", {'raw_input_list':['login','view 24','prev','next','next','next','next','next','next','exit'],
  'input_list':login_details})
path3 = ("login_exit", {'raw_input_list':['login','exit'],
  'input_list':login_details})
path4 = ("login_twice", {'raw_input_list':['login','view 24', 'login', 'all', 'exit'],
  'input_list':login_details+login_details})
path5 = ("login_exit", {'raw_input_list':['login','login', 'all', 'exit'],
  'input_list':['wrongsubdomain','emailaddress@gmail.com','notmypassword']+login_details})
path6 = ("blank_login", {'raw_input_list':['login', 'login', 'exit'],
  'input_list':['  ','somesubdomain', '    ', 'email@sampleaddress.com','notmypassword']+login_details})

class TestSequencesWithLogin(object):
    paths = [path1, path2, path3, path4, path5, path6]

    def test_paths(self, raw_input_list, input_list):
      with patch('ticket_viewer.get_input') as mock_input:
        with patch('builtins.input') as mock_raw_input:
          mock_raw_input.side_effect = raw_input_list
          mock_input.side_effect = input_list
          with captured_output() as (out, err):
            ticket_viewer.TicketViewer().cmdloop()
          output = out.getvalue().strip().split('\n')
          for s in output:
            print(s)
          assert output[0] == intro
          assert output[-1] == outro
