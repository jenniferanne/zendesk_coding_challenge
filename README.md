# Ticket Viewer

This is an application developed by Jennifer Ralph for the Zendesk Internship Coding Challenge

---

## Language and dependencies

This tool is developed in Python 3, and is designed to be run in the command line. It has the following requirements:

- setuptools (ver 39.0.1)
- requests (ver 2.22.0)
- mock (ver 3.0.5)

The program is executed by entering "python3 ticket_viewer.py" on the command line in the directory in which ticket_viewer.py is located.


---

## Usage

The program is designed to be user friendly, and offers the user valid options such as "login", "all" or "view". 
Within the program, the user can type "?" to view the list of available commands, and can type "help {command}" to read a description of the given command.


---

##Testing

Testing is performed with pytest-3. Please note that the file "test_paths_with_login.py" has login details that have been replaced with placeholders. This is to prevent uploading account details onto github. You will need to replace the placeholders with valid account details if you want to use the tests.